import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../models/Usuario';
import { JwtResponse } from '../models/JwtResponse';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private urlEndPoint: string;
  private _usuario: Usuario;
  private _token: string;
  constructor(private http: HttpClient) {
    this.urlEndPoint = `${environment.urlApi}/api/auth/signinWeb`;
  }

  signInWeb(credentials: Usuario): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.urlEndPoint, credentials);
  }

  guardarToken(accessToken: string): void {
    this._token = accessToken;
    localStorage.setItem('token', accessToken);
  }

  guardarUsuario(usuario: Usuario, avatar: string){
    this._usuario = new Usuario();
    this._usuario.usuarioId = usuario.usuarioId;
    this._usuario.nombres = usuario.nombres;
    this._usuario.email = usuario.email;
    this._usuario.imageAvatar = avatar;
    this._usuario.avatar = usuario.avatar == null ? '' : usuario.avatar;
    localStorage.setItem('usuario', JSON.stringify(this._usuario));
  }

  public get getToken(): string {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && localStorage.getItem('token') != null) {
      this._token = localStorage.getItem('token');
      return this._token;
    }
    return null;
  }

  public get getUsuario(): Usuario {
    if (this._usuario != null) {
      return this._usuario;
    } else if (this._usuario == null && localStorage.getItem('usuario') != null) {
      this._usuario = JSON.parse(localStorage.getItem('usuario')) as Usuario;
      return this._usuario;
    }
    return new Usuario();
  }

  isAuthenticated(): boolean {
    if (this.getToken != null && this.getToken != '') {
      return true;
    }
    return false;
  }

  logout(): void {
    localStorage.clear();
  }
}
