import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Universidad } from '../models/Universidad';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UniversidadService {
  private urlEndPoint: string;
  constructor(private http: HttpClient) {
    this.urlEndPoint = `${environment.urlApi}/api/universidad`;
  }

  listarUniversidades(): Observable<Universidad[]> {
    return this.http.get<Universidad[]>(`${this.urlEndPoint}/listarUniversidadesWeb`);
  }
}
