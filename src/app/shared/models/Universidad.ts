export class Universidad {
  universidadId: number;
  nombre: string;
  direccion: string;
  rutaLogo: string;
  rutaSede: string;
  estado: string;
  nombreCorto: string;
  descripcion: string;
  slide1: string;
  slide2: string;
  slide3: string;
}
