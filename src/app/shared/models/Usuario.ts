export class Usuario {
  usuarioId: number;
  nombres: string;
  email: string;
  clave: string;
  nroDocumento: string;
  fechNacimiento: Date;
  idDistrito: string;
  idColegio: string;
  anioFinColegio: number;
  estado: string;
  telefono: string;
  nomDistrito: string;
  alias: string;
  avatar: string;
  imageAvatar: string;
  esAdministrador: boolean;
  nuevaClave: string;
}
