import { Usuario } from './Usuario';

export class JwtResponse {
  token: string;
  type: string;
  usuario: Usuario;
  authorities: string[];
}
