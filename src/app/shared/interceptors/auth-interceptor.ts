import { HttpHandler, HttpRequest, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, empty } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError, retry, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,
              private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    return next.handle(req).
    pipe(
      retry(1),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            //errorMessage = `Error: ${error.error.message}`;

            if (error.status === 401) {
              // console.log('401 client side');
              errorMessage = 'Usuario o contraseña no válida';
            }
            if (error.status === 403) {
              // console.log('403 client side');
            }
          } else {
            // server-side error
            if (error.status === 401) {
              // console.log('401 server side');
              errorMessage = 'Usuario o contraseña no válida';
              this.authService.logout();
            }
            if (error.status === 403) {
              // console.log('403 server side');
            }
            //errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            if (error.status === 0) {
              errorMessage = 'No se pudo establecer la conexión';
            }
          }
          return throwError(errorMessage);
        }),
    );
  }
}
