import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';
import { NgxLoadingModule } from 'ngx-loading';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    LoaderComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgxLoadingModule.forRoot({})
  ],
  exports: [
    LoaderComponent,
    SidebarComponent
  ]
})
export class ComponentsModule { }
