import { Component, OnInit, Renderer2, ViewChild, ElementRef, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Usuario } from 'src/app/shared/models/Usuario';
import { AuthService } from 'src/app/shared/services/auth.service';
import Swal from 'sweetalert2';
import { timer } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  enviado = false;
  loginCorrecto = false;
  usuarioNombre = '';
  @ViewChild('form') form: ElementRef;
  @ViewChild('wrapper') wrapper: ElementRef;
  constructor(private render: Renderer2,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  login() {
    this.enviado = true;
    if (!this.loginForm.valid) {
      return ;
    }
    const usuario = new Usuario();
    usuario.email = this.loginForm.controls.email.value;
    usuario.clave = this.loginForm.controls.password.value;

    this.authService.signInWeb(usuario).subscribe(
      (response) => {
        if (response.usuario.esAdministrador) {
          this.authService.guardarToken(response.token);
          this.authService.guardarUsuario(response.usuario, '');
          this.usuarioNombre = response.usuario.nombres;
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Usuario sin autorización',
          });
        }
      }, (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error
        });
      }, () => {
        this.loginCorrecto = true;
        this.render.addClass(this.form.nativeElement, 'fadeOut');
        this.render.addClass(this.wrapper.nativeElement, 'form-success');
        setTimeout(() => {
          this.irAViews();
        }, 1500);
      }
    );
  }

  irAViews() {
    this.router.navigate(['/views']);
  }
}
