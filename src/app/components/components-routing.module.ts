import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsComponent } from './components.component';
import { PresentacionComponent } from './presentacion/presentacion.component';
import { PreguntasComponent } from './preguntas/preguntas.component';
import { UniversidadesComponent } from './universidades/universidades.component';
import { CarrerasComponent } from './carreras/carreras.component';

const routes: Routes = [
  { path: '', component: ComponentsComponent,
  children: [
    { path: '', redirectTo: 'presentacion', pathMatch: 'prefix' },
    { path: 'presentacion', component: PresentacionComponent },
    { path: 'preguntas', component: PreguntasComponent },
    { path: 'universidades', component: UniversidadesComponent },
    { path: 'carreras', component: CarrerasComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
