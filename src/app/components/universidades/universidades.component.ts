import { Component, OnInit } from '@angular/core';
import { UniversidadService } from 'src/app/shared/services/universidad.service';
import { Universidad } from 'src/app/shared/models/Universidad';

@Component({
  selector: 'app-universidades',
  templateUrl: './universidades.component.html',
  styleUrls: ['./universidades.component.sass']
})
export class UniversidadesComponent implements OnInit {
  listaUniversidades: Universidad[];
  constructor(private universidadService: UniversidadService) {
    this.listaUniversidades = this.listaUniversidades || [];
  }

  ngOnInit(): void {
    this.universidadService.listarUniversidades().subscribe(
      (response) => {
        this.listaUniversidades = response;
      }, (error) => console.log(error)
    );
  }

}
