import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.sass']
})
export class ComponentsComponent implements OnInit {
  hovered = false;
  constructor() { }

  ngOnInit(): void {
  }

  pushContent(event: any) {
    this.hovered = event;
  }
}
