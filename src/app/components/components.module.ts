import { NgModule } from '@angular/core';

import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsComponent } from './components.component';
import { SharedModule } from '../shared/shared.module';
import { PresentacionComponent } from './presentacion/presentacion.component';
import { PreguntasComponent } from './preguntas/preguntas.component';
import { UniversidadesComponent } from './universidades/universidades.component';
import { CarrerasComponent } from './carreras/carreras.component';


@NgModule({
  declarations: [
    ComponentsComponent,
    PresentacionComponent,
    PreguntasComponent,
    UniversidadesComponent,
    CarrerasComponent
  ],
  imports: [
    ComponentsRoutingModule,
    SharedModule
  ]
})
export class ComponentsModule { }
